###
# Mastermind game implementaion played through command line
# Game.play runs game

class Code
  # Represents sequence of 4 pegs
  attr_reader :pegs

  PEGS = {
    red: 'r',
    green: 'g',
    blue: 'b',
    yellow: 'y',
    orange: 'o',
    purple: 'p'
  }.freeze

  def initialize(pegs)
    @pegs = pegs
  end

  # why is [] spec showing @ pegs as [['bbbb'],['bbbb'],['bbbb'],['bbbb']]
  def [](index)
    @pegs[index]
  end
  # Build instance with random peg colors
  def self.random
    rand_array = []
    4.times do
      rand_array << PEGS.values[rand(0..5)]
    end
    Code.new(rand_array)
  end

  # Take user input string like "RGBY" and build Code object
  # Make code objects for both secret code and user guess
  def self.parse(input)
    unless input.chars.all? { |char| PEGS.values.include?(char.downcase) }
      raise 'invalid color'
    end
    raise 'please input four colors' unless input.length == 4
    Code.new(input.chars)
  end

  def exact_matches(other_code)
    exact_count = 0
    @pegs.each_with_index do |color, idx|
      exact_count += 1 if color == other_code[idx]
    end
    exact_count
  end

  def near_matches(other_code)
    near_count = 0
    other_code.pegs.uniq.each_with_index do |color, idx|
      if @pegs.include?(color)
        if @pegs.count(color) < other_code.pegs.count(color)
          near_count += @pegs.count(color)
        elsif @pegs.count(color) >= other_code.pegs.count(color)
          near_count += other_code.pegs.count(color)
        end
      end
    end
    near_count - exact_matches(other_code)
  end

  def ==(other_code)
    return false if self.class != other_code.class
    @pegs.map(&:downcase) == other_code.pegs.map(&:downcase)
  end
end

class Game
  # Keeps track of: how many turns passed the correct code
  attr_reader :secret_code

  def initialize(code = nil)
    @secret_code = if code.nil?
                     Code.random
                   else
                     code
                   end
    @turns = 1
  end

  # Prompts user for input
  def get_guess
    p 'What is your guess? (rgbyop)'
    input = gets.chomp

    @guess = Code.parse(input)
  end

  def display_matches(code)
    p 'near matches: ' + @secret_code.near_matches(code).to_s
    p 'exact matches: ' + @secret_code.exact_matches(code).to_s
  end

  def play
    if @turns >= 10
      p "You lose the code was #{@secret_code.pegs}"
      return false
    end
    puts "\n Turn number: #{@turns}"
    get_guess
    display_matches(@guess)
    @turns += 1
    if @guess == @secret_code
      p 'You Win!'
      return true
    else
      play
    end
  end
end
